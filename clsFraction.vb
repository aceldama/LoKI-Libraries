Namespace Global.LoKI.AceldamA.Mathematics
    ''' <summary>A Fraction class.</summary>
    ''' <remarks>
    '''     Fraction Glossary:  https://www.ducksters.com/kidsmath/fractions_glossary.php
    '''     Decimal Datatype:   https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/decimal-data-type
    '''     Operator Overloads: http://msdn.microsoft.com/en-us/library/orm-9780596518431-01-12.aspx
    ''' </remarks>
    Public Class Fraction
#Region "Global Vars"
        '-- Private
        Private _Num As Long        '-- Numerator
        Private _Den As Long        '-- Denominator
        Private _Recalc As Boolean  '-- Recalc Optimization Flag
        Private _Dec As Decimal     '-- Decimal value of the fraction
        Private _Inv As Decimal     '-- Inverted decimal (Dec * Inv = 1)
#End Region


#Region "Constructor"
        ''' <summary>Creates a new fraction (Default value = 0)</summary>
        Public Sub New()
            Me.New(0, 1)
        End Sub

        ''' <summary>Creates a new fraction with the provided numerator/denominator</summary>
        ''' <param name="Numerator">Top part of fraction</param>
        ''' <param name="Denominator">Bottom part of ftraction</param>
        Public Sub New(ByVal Numerator As Long, ByVal Denominator As Long)
            Me.Numerator = Numerator
            Me.Denominator = Denominator
        End Sub

        ''' <summary>Creates a new fraction with the provided decimal value</summary>
        ''' <param name="Number">Value to create the fraction from.</param>
        Public Sub New(ByVal Number As Decimal)
            Number = Math.Round(Number, 10)
            Me._Den = 1
            Do While (Number Mod 1) > 0
                Number = Number * 10
                Me._Den *= 10
            Loop
            Me.Numerator = Math.Floor(Number)
            Me.Simplify()
        End Sub

        ''' <summary>Creates a new fraction from a fraction provided</summary>
        ''' <param name="Fract">The fraction to clone</param>
        Public Sub New(ByVal Fract As Fraction)
            Me.New(Fract.Numerator, Fract.Denominator)
        End Sub
#End Region


#Region "Properties"
        ''' <summary>The top part of the fraction</summary>
        ''' <returns>The numerator as Long</returns>
        Public Property Numerator() As Long
            Get
                Return Me._Num
            End Get
            Set(ByVal Value As Long)
                If (Me._Num <> Value) AndAlso (Value <> 0) Then
                    Me._Recalc = True
                    Me._Num = Value
                End If
            End Set
        End Property

        ''' <summary>The bottom part of the fraction</summary>
        ''' <returns>The denominator as Long</returns>
        Public Property Denominator() As Long
            Get
                Return Me._Den
            End Get
            Set(ByVal Value As Long)
                If (Me._Num <> Value) AndAlso (Value <> 0) Then
                    Me._Recalc = True
                    Me._Den = Value
                End If
            End Set
        End Property

        ''' <summary>The fraction expressed as a decimal value</summary>
        ''' <returns>The decimal value of the fraction</returns>
        Public ReadOnly Property [Decimal]() As Decimal
            Get
                Me.fRecalculate()
                Return Me._Dec
            End Get
        End Property

        ''' <summary>The reciprocal of the fraction expressed as a decimal value</summary>
        ''' <returns>the reciprocal decimal vlaue of the fraction (1/x) as a decimal</returns>
        Public ReadOnly Property Reciprocal() As Decimal
            Get
                Me.fRecalculate()
                Return Me._Inv
            End Get
        End Property
#End Region


#Region "Function Overloads"
        ''' <summary>Casts the fraction to a string value</summary>
        ''' <returns>A string representing the fraction</returns>
        Public Overrides Function ToString() As String
            Return String.Format("{0}/{1}", Me._Num, Me._Den)
        End Function
#End Region


#Region "Delegates"
        ''' <summary>Used to pass the LCM and GCD function address to fGetLCMGCD</summary>
        ''' <param name="NumA">First number</param>
        ''' <param name="NumB">Second number</param>
        ''' <returns>An integer containing either the LCM or GCD</returns>
        Private Delegate Function CalcDelegate(ByVal NumA As Integer, ByVal NumB As Integer) As Integer
#End Region


#Region "Methods"
        '-- Private
        ''' <summary>Optimisation routine mainly for use in the object's property values</summary>
        Private Sub fRecalculate()
            If Me._Recalc Then Exit Sub
            Me._Dec = IIf(Me._Den = 0, Nothing, Me._Num / Me._Den)
            Me._Inv = IIf(Me._Num = 0, Nothing, Me._Den / Me._Num)
            Me._Recalc = False
        End Sub

        ''' <summary>Gets the LCM or GCD between the numbers provided</summary>
        ''' <param name="NumA">First number</param>
        ''' <param name="NumB">Second number</param>
        ''' <param name="calc">LCM or GCD function</param>
        ''' <param name="NumN">Rest of the numbers</param>
        ''' <returns>The LCM or GCD as an integer</returns>
        Private Shared Function fGetLCMGCD(ByVal NumA As Integer, ByVal NumB As Integer, ByVal calc As CalcDelegate, ByVal ParamArray NumN() As Integer)
            Dim arrUnique() As Integer = NumN.Distinct()
            Dim tValue = calc(NumA, NumB)

            Dim I As Integer = 0
            Do While (I < arrUnique.Length)
                If Not ((arrUnique(I) = 0) Or (arrUnique(I) = tValue)) Then
                    '-- Get the new value
                    tValue = calc(tValue, arrUnique(I))
                End If
                I = I + 1
            Loop

            Return tValue
        End Function


        '-- Public 
        ''' <summary>Gets the greatest common divisor between two numbers</summary>
        ''' <param name="NumA">Larger of the two numbers</param>
        ''' <param name="NumB">Smaller of the two numbers</param>
        ''' <returns>The GCD as Integer</returns>
        ''' <remarks>
        '''     The GCD works its way down to 1 from a number LEQ min(a, b)
        '''     Ref: https://en.wikipedia.org/wiki/Greatest_common_divisor
        ''' </remarks>
        Public Shared Function GetGCD(ByVal NumA As Integer, ByVal NumB As Integer) As Integer
            Dim temp As Integer
            If NumB > NumA Then
                temp = NumA
                NumB = NumA
                NumA = temp
            End If

            While NumB <> 0
                temp = NumA Mod NumB
                NumA = NumB
                NumB = temp
            End While

            Return NumA
        End Function

        ''' <summary>Gets the greatest common divisor between multiple numbers</summary>
        ''' <param name="NumA">First number</param>
        ''' <param name="NumB">Second number</param>
        ''' <param name="NumN">Rest of the numbers</param>
        ''' <returns>The GCD as an integer</returns>
        Public Shared Function GetGCD(ByVal NumA As Integer, ByVal NumB As Integer, ByVal ParamArray NumN() As Integer) As Integer
            Return Fraction.fGetLCMGCD(NumA, NumB, AddressOf Fraction.GetGCD, NumN)
        End Function

        ''' <summary>Gets least common multiple between two numbers</summary>
        ''' <param name="NumA">First number</param>
        ''' <param name="NumB">Second number</param>
        ''' <returns>The LCM as an integer</returns>
        ''' <remarks>
        '''     The LCM is the smallest positive integer that is divisible by the numbers provided
        '''     Ref: https://en.wikipedia.org/wiki/Least_common_multiple
        ''' </remarks>
        Public Shared Function GetLCM(ByVal NumA As Integer, ByVal NumB As Integer) As Integer
            Return ((NumA * NumB) \ Fraction.GetGCD(NumA, NumB))
        End Function

        ''' <summary>Gets the least common multiple between multiple numbers</summary>
        ''' <param name="NumA">First number</param>
        ''' <param name="NumB">Second number</param>
        ''' <param name="NumN">Rest of the numbers</param>
        ''' <returns>The LCM as an integer</returns>
        Public Shared Function GetLCM(ByVal NumA As Integer, ByVal NumB As Integer, ByVal ParamArray NumN() As Integer) As Integer
            Return Fraction.fGetLCMGCD(NumA, NumB, AddressOf Fraction.GetLCM, NumN)
        End Function

        ''' <summary>Simplifies the fraction by dividing the numerator and denominator with their GCD</summary>
        Public Sub Simplify()
            Dim tGCD As Integer = Fraction.GetGCD(Me._Num, Me._Den)
            If (tGCD > 1) Then
                Me._Num = (Me._Num \ tGCD)
                Me._Den = (Me._Den \ tGCD)
            End If
            Me._Recalc = True
        End Sub

        ''' <summary>Adds a value to the fraction</summary>
        ''' <param name="Num">The value to add</param>
        ''' <remarks>
        '''     Ref: http://www.themathpage.com/arith/add-fractions-subtract-fractions-1.htm
        ''' </remarks>
        Public Sub Add(ByVal Num As Long)
            Me.Numerator = Me._Num + Num
            Me.Simplify()
        End Sub

        ''' <summary>Adds a value to the fraction</summary>
        ''' <param name="Fraction">The value to add<</param>
        Public Sub Add(ByVal Fraction As Fraction)
            If Me.Denominator <> Fraction.Denominator Then
                Me.Numerator = (Me._Num * Fraction.Denominator) + (Fraction.Numerator * Me._Den)
                Me.Denominator = Me._Den * Fraction.Denominator
            Else
                Me.Numerator = Me._Num + Fraction.Numerator
            End If
            Me.Simplify()
        End Sub

        ''' <summary>Adds mutiple fractions to the fraction</summary>
        ''' <param name="Fraction">The fractions to add</param>
        Public Sub Add(ByVal ParamArray Fraction() As Fraction)
            For Each tF As Fraction In Fraction
                Me.Add(tF)
            Next
        End Sub

        ''' <summary>Subtracts a value from the fraction</summary>
        ''' <param name="Num">The value to subtract</param>
        Public Sub Subtract(ByVal Num As Long)
            Me.Add(-Num)
        End Sub

        ''' <summary>Subtracts a value from the fraction</summary>
        ''' <param name="Fraction">The value to subtract</param>
        Public Sub Subtract(ByVal Fraction As Fraction)
            If Me.Denominator <> Fraction.Denominator Then
                Me.Numerator = (Me._Num * Fraction.Denominator) - (Fraction.Numerator * Me._Den)
                Me.Denominator = Me._Den * Fraction.Denominator
            Else
                Me.Numerator = Me._Num + Fraction.Numerator
            End If
            Me.Simplify()
        End Sub

        ''' <summary>Subtracts multiple fractions from the fraction</summary>
        ''' <param name="Fraction">The fractions to subtract</param>
        Public Sub Subtract(ByVal ParamArray Fraction() As Fraction)
            For Each tF As Fraction In Fraction
                Me.Subtract(tF)
            Next
        End Sub

        ''' <summary>Multiplies the fraction by a given value</summary>
        ''' <param name="Num">The value to multiply the fraction by</param>
        ''' <remarks>
        '''     Ref: http://www.themathpage.com/arith/multiply-fractions-divide-fractions.htm
        ''' </remarks>
        Public Sub Multiply(ByVal Num As Long)
            Me.Numerator = Me._Num * Num
            Me.Simplify()
        End Sub

        ''' <summary>Multiplies the fraction by a given value</summary>
        ''' <param name="Fraction">The value to multiply the fraction by</param>
        Public Sub Multiply(ByVal Fraction As Fraction)
            Me.Numerator = Me._Num * Fraction.Numerator
            Me.Denominator = Me._Den * Fraction.Denominator
            Me.Simplify()
        End Sub

        ''' <summary>Multiplies the fraction by other fractions</summary>
        ''' <param name="Fraction">The fractions to multiply the fraction by</param>
        Public Sub Multiply(ByVal ParamArray Fraction() As Fraction)
            For Each tF As Fraction In Fraction
                Me.Multiply(tF)
            Next
        End Sub

        ''' <summary>Divides the fraction by a given value</summary>
        ''' <param name="Num">The value to divide the fraction by</param>
        ''' <remarks>
        '''     Ref: http://www.themathpage.com/arith/multiply-fractions-divide-fractions-2.htm
        ''' </remarks>
        Public Sub Divide(ByVal Num As Long)
            Me.Denominator = Me._Den * Num
            Me.Simplify()
        End Sub

        ''' <summary>Divides the fraction by a given value</summary>
        ''' <param name="Fraction">The value to divide the fraction by</param>
        Public Sub Divide(ByVal Fraction As Fraction)
            Me.Numerator = Me._Num * Fraction.Denominator
            Me.Denominator = Me._Den * Fraction.Numerator
            Me.Simplify()
        End Sub

        ''' <summary>Divides the fraction by other fractions</summary>
        ''' <param name="Fraction">The fractions to divide the fraction by</param>
        Public Sub Divide(ByVal ParamArray Fraction() As Fraction)
            For Each tF As Fraction In Fraction
                Me.Divide(tF)
            Next
        End Sub

        ''' <summary>Raises the fraction to the exponent provided</summary>
        ''' <param name="Exponent">Exponent to raise the fraction to</param>
        ''' <remarks> 
        '''     Ref: http://www.wikihow.com/Square-Fractions 
        ''' </remarks>
        Public Sub Exp(ByVal Exponent As Byte)
            Me.Simplify()
            Dim tN As Integer = Me._Num
            Dim tD As Integer = Me._Den
            For I As Integer = 1 To Exponent
                Me.Denominator = Me._Num * tN
                Me.Denominator = Me._Den * tD
                Me.Simplify()
            Next
        End Sub
#End Region


#Region "Operator Overloads"
        '-- Unary Arithmetic Operator Overloads
        Public Shared Operator +(ByVal Fract As Fraction) As Fraction
            Return New Fraction(Fract.Numerator, Fract.Denominator)
        End Operator

        Public Shared Operator -(ByVal Fract As Fraction) As Fraction
            Return New Fraction(-Fract.Numerator, Fract.Denominator)
        End Operator

        '-- Binary Arithmetic Operator Overloads
        Public Shared Operator +(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Add(Fract2)
            Return tFract
        End Operator

        Public Shared Operator +(ByVal Fract As Fraction, ByVal Num As Long) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Add(Num)
            Return tFract
        End Operator

        Public Shared Operator -(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Subtract(Fract2)
            Return tFract
        End Operator

        Public Shared Operator -(ByVal Fract As Fraction, ByVal Num As Long) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Subtract(Num)
            Return tFract
        End Operator

        Public Shared Operator *(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Multiply(Fract2)
            Return tFract
        End Operator

        Public Shared Operator *(ByVal Fract As Fraction, ByVal Num As Long) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Multiply(Num)
            Return tFract
        End Operator

        Public Shared Operator /(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Divide(Fract2)
            Return tFract
        End Operator

        Public Shared Operator /(ByVal Fract As Fraction, ByVal Num As Long) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Divide(Num)
            Return tFract
        End Operator

        Public Shared Operator ^(ByVal Fract As Fraction, ByVal Num As Byte) As Fraction
            Dim tFract As Fraction = New Fraction(Fract.Numerator, Fract.Denominator)
            tFract.Exp(Num)
            Return tFract
        End Operator

        '-- String Concatenation Overload
        Public Shared Operator &(ByVal Fract As Fraction, ByVal tStr As String) As String
            Return Fract.ToString() & tStr
        End Operator

        '-- CType Operator Overloads
        Public Shared Widening Operator CType(ByVal Fract As Fraction) As Decimal
            Return Fract.Decimal
        End Operator

        Public Shared Widening Operator CType(ByVal Fract As Fraction) As String
            Return Fract.ToString
        End Operator

        '-- Logic Operator Overloads
        '-- EQU, NEQ
        Public Shared Operator =(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Boolean
            Return Fract.Decimal = Fract2.Decimal
        End Operator

        Public Shared Operator <>(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Boolean
            Return Fract.Decimal <> Fract2.Decimal
        End Operator

        Public Shared Operator =(ByVal Fract As Fraction, ByVal Number As Long) As Boolean
            Return Fract.Decimal = Number
        End Operator

        Public Shared Operator <>(ByVal Fract As Fraction, ByVal Number As Long) As Boolean
            Return Fract.Decimal <> Number
        End Operator

        Public Shared Operator =(ByVal Fract As Fraction, ByVal Number As Decimal) As Boolean
            Return Fract.Decimal = Number
        End Operator

        Public Shared Operator <>(ByVal Fract As Fraction, ByVal Number As Decimal) As Boolean
            Return Fract.Decimal <> Number
        End Operator

        '-- LSS, GTR
        Public Shared Operator <(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Boolean
            Return Fract.Decimal < Fract2.Decimal
        End Operator

        Public Shared Operator >(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Boolean
            Return Fract.Decimal > Fract2.Decimal
        End Operator

        Public Shared Operator <(ByVal Fract As Fraction, ByVal Number As Long) As Boolean
            Return Fract.Decimal < Number
        End Operator

        Public Shared Operator >(ByVal Fract As Fraction, ByVal Number As Long) As Boolean
            Return Fract.Decimal > Number
        End Operator

        Public Shared Operator <(ByVal Fract As Fraction, ByVal Number As Decimal) As Boolean
            Return Fract.Decimal < Number
        End Operator

        Public Shared Operator >(ByVal Fract As Fraction, ByVal Number As Decimal) As Boolean
            Return Fract.Decimal > Number
        End Operator

        '-- LEQ, GEQ
        Public Shared Operator <=(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Boolean
            Return Fract.Decimal <= Fract2.Decimal
        End Operator

        Public Shared Operator >=(ByVal Fract As Fraction, ByVal Fract2 As Fraction) As Boolean
            Return Fract.Decimal >= Fract2.Decimal
        End Operator

        Public Shared Operator <=(ByVal Fract As Fraction, ByVal Number As Long) As Boolean
            Return Fract.Decimal <= Number
        End Operator

        Public Shared Operator >=(ByVal Fract As Fraction, ByVal Number As Long) As Boolean
            Return Fract.Decimal >= Number
        End Operator

        Public Shared Operator <=(ByVal Fract As Fraction, ByVal Number As Decimal) As Boolean
            Return Fract.Decimal <= Number
        End Operator

        Public Shared Operator >=(ByVal Fract As Fraction, ByVal Number As Decimal) As Boolean
            Return Fract.Decimal >= Number
        End Operator
#End Region
    End Class


    Public Class MixedNumber : Inherits Fraction
        ''' <summary>The number of whole number of the mixed number</summary>
        ''' <returns>A whole number value as a long</returns>
        Public Property MixedNumberWhole() As Long
            Get
                Return Me.Numerator \ Me.Denominator
            End Get
            Private Set(ByVal Value As Long)
                '-- Private until i figure out a stable, idiot-proof way to do this
                Me.Numerator = Me.Denominator * Value
            End Set
        End Property

        ''' <summary>Returns the numerator of a mixed number (ie. where denominator > numerator)</summary>
        ''' <returns>The numerator as a long</returns>
        Public ReadOnly Property MixedNumberNumerator As Long
            Get
                Return Me.Numerator Mod Me.Denominator
            End Get
        End Property

        ''' <summary>Returns the fraction part of a mixed number</summary>
        ''' <returns>The fraction as a new Fraction object</returns>
        Public ReadOnly Property MixedNumberFraction As Fraction
            Get
                Return New Fraction(Me.MixedNumberNumerator, Me.Denominator)
            End Get
        End Property


        Public Overrides Function ToString() As String
            Return String.Format("{2}{0}/{1}", Me.MixedNumberNumerator, Me.Denominator, IIf(Me.MixedNumberWhole <> 0, String.Format("{0} & ", Me.MixedNumberWhole), ""))
        End Function
    End Class
End Namespace
