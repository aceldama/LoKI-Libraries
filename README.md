# LoKI-Libraries

## clsFraction.vb
Fraction math class for use in VB.net (2017)
``` vb
Imports System.Diagnostics
Imports LoKI.AceldamA.Mathematics

Class Test
    Public Sub Main()
        Dim fract as New Fraction(6, 4)

        Debug.Print(fract * 5)
        Debug.Print(fract - 2)
    End Sub
End Class
```
